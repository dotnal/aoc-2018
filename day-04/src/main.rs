use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let f = File::open("input").expect("error opening file");
    let reader = BufReader::new(f);

    let mut sorted_input: Vec<String> = Vec::new();
    let mut guards: HashMap<i32, Guard> = HashMap::new();

    for line in reader.lines().filter_map(|i| i.ok()) {
        sorted_input.push(line);
    }
    sorted_input.sort_unstable();

    let mut active_guard_id: i32 = 0;
    let mut sleep_start: Option<i32> = None;

    for line in sorted_input {
        if line.contains("Guard") {
            active_guard_id = parse_guard_id(&line);
            guards
                .entry(active_guard_id)
                .or_insert_with(|| Guard::new(active_guard_id));

        } else if line.contains("asleep") {
            sleep_start = Some(parse_time(&line));

        } else if line.contains("wakes") {
            match sleep_start {
                Some(start) => {
                    let sleep_end = parse_time(&line);
                    let duration = Duration::from_sleep(start, sleep_end);
                    guards
                        .get_mut(&active_guard_id)
                        .unwrap()
                        .add_sleep_duration(duration);
                }
                None => panic!("what the fuck"),
            }
        }
    }

    println!("part one: {}", part_one(&guards));
    println!("part two: {}", part_two(&guards));
}

fn part_one(guards: &HashMap<i32, Guard>) -> i32 {
    let max_guard = guards
        .iter()
        .map(|(_, g)| g)
        .max_by_key(|g| g.get_total_sleep())
        .unwrap();

    max_guard.id * max_guard.get_highest_freq_minute().0
}

fn part_two(guards: &HashMap<i32, Guard>) -> i32 {
    let guard = guards
        .iter()
        .max_by_key(|(_, v)| v.get_highest_freq_minute().1)
        .map(|(_, v)| v)
        .unwrap();

    guard.id * guard.get_highest_freq_minute().0 // can't map above, lifetime issues ahoy
}

fn parse_guard_id(input: &str) -> i32 {
    input
        .split('#')
        .nth(1)
        .unwrap()
        .split_whitespace()
        .next()
        .unwrap()
        .parse::<i32>()
        .unwrap()
}

fn parse_time(input: &str) -> i32 {
    input
        .split_whitespace()
        .nth(1)
        .unwrap()
        .split(']')
        .nth(0)
        .unwrap()
        .split(':')
        .nth(1)
        .unwrap()
        .parse::<i32>()
        .unwrap()
}

#[derive(Debug)]
struct Duration {
    minutes: i32,
    sleep_minutes: Vec<i32>,
}

impl Duration {
    fn from_sleep(start: i32, end: i32) -> Duration {
        Duration {
            minutes: end - start,
            sleep_minutes: (start..end).collect(),
        }
    }
}

type Minute = i32;
type Frequency = i32;

#[derive(Debug)]
struct Guard {
    pub id: i32,
    pub sleep_durations: Vec<Duration>,
}

impl Guard {
    fn new(id: i32) -> Guard {
        Guard {
            id,
            sleep_durations: Vec::new(),
        }
    }

    fn add_sleep_duration(&mut self, duration: Duration) {
        self.sleep_durations.push(duration);
    }

    fn get_total_sleep(&self) -> i32 {
        self.sleep_durations.iter().map(|d| d.minutes).sum()
    }

    fn get_highest_freq_minute(&self) -> (Minute, Frequency) {
        let mut numbers = HashMap::new();

        self.sleep_durations
            .iter()
            .flat_map(|d| d.sleep_minutes.clone())
            .for_each(|min| {
                *numbers.entry(min).or_insert(0) += 1;
            });

        let result = numbers
            .iter()
            .max_by_key(|&(_, count)| count)
            .unwrap_or_else(|| (&0, &0)); //some guards don't sleep apparently...

        (*result.0, *result.1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_guard_parse() {
        let guard_string = "[1518-03-21 00:00] Guard #1759 begins shift".to_string();
        let id = parse_guard_id(&guard_string);

        assert_eq!(id, 1759);
    }

    #[test]
    fn test_minute_parse() {
        let time_string = "[1518-11-13 00:25] falls asleep".to_string();
        let time = parse_time(&time_string);

        assert_eq!(time, 25);
    }
}
