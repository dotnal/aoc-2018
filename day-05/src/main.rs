use std::collections::HashMap;
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let f = File::open("input").expect("error opening file");
    let reader = BufReader::new(f);
    let mut polymer = VecDeque::new();
    for line in reader.lines().filter_map(|i| i.ok()) {
        line.trim().chars().for_each(|c| polymer.push_back(c));
    }

    println!("part one: {}", part_one(&polymer));
    println!("part two: {}", part_two(&polymer));
}

fn part_one(polymer: &VecDeque<char>) -> usize {
    let old_polymer = polymer.clone();
    fully_sanitise(old_polymer).len() 
}

fn part_two(polymer: &VecDeque<char>) -> usize {
    let alphabet = (0..26).map(|c| (c + 'a' as u8) as char).collect::<Vec<_>>();
    let mut size_map: HashMap<char, usize> = HashMap::new();

    for c in alphabet {
        let poly = fully_sanitise(delete_molecule(polymer.clone(), c));
        size_map.insert(c, poly.len());
    }

    *size_map.iter().min_by_key(|(_, &v)| v).unwrap().1
}

fn delete_molecule(mut old_polymer: VecDeque<char>, molecule: char) -> VecDeque<char> {
    old_polymer
        .iter()
        .filter(|c| c.to_ascii_lowercase() != molecule.to_ascii_lowercase())
        .map(|c| *c)
        .collect()
}

fn fully_sanitise(mut old_polymer: VecDeque<char>) -> VecDeque<char> {
    let old_len = old_polymer.len();
    let mut new_polymer: VecDeque<char> = VecDeque::new();
    let mut current_molecule = old_polymer.pop_front();

    loop {
        if current_molecule == None {
            break;
        }

        if old_polymer.is_empty() {
            if let Some(i) = current_molecule {
                new_polymer.push_back(i)
            }
            break;
        }

        if determine_reaction(current_molecule.unwrap(), *old_polymer.front().unwrap()) {
            old_polymer.pop_front();
        } else {
            new_polymer.push_back(current_molecule.unwrap());
        }

        current_molecule = old_polymer.pop_front();
    }

    if new_polymer.len() == old_len {
        return new_polymer;
    }

    fully_sanitise(new_polymer)
}

fn determine_reaction(a: char, b: char) -> bool {
    if a.is_lowercase() == b.is_lowercase() {
        return false;
    }
    match a.is_uppercase() {
        true => a == b.to_ascii_uppercase(),
        false => a == b.to_ascii_lowercase(),
    }
}

mod tests {
    use super::*;
    #[test]
    fn target_sanitise_molecule() {
        let alphabet = (0..26).map(|c| (c + 'a' as u8) as char).collect::<Vec<_>>();
        let mut polymer = VecDeque::new();

        for l in alphabet {
            polymer.push_back(l);
        }
        let new_polymer = delete_molecule(polymer, 'A');
        for m in new_polymer {
            assert_eq!(m != 'a', true);
        }

    }

    #[test]
    fn test_determine_reaction() {
        let a: char = 'a';
        let a_upper: char = 'A';

        assert_eq!(determine_reaction(a, a_upper), true);
    }

    #[test]
    fn test_determine_non_reaction() {
        let a: char = 'a';
        let a_upper: char = 'a';

        assert_eq!(determine_reaction(a, a_upper), false);
    }

    #[test]
    fn test_determine_non_reaction_letter() {
        let a: char = 'a';
        let a_upper: char = 'B';

        assert_eq!(determine_reaction(a, a_upper), false);
    }
}
