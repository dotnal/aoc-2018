extern crate regex;
use regex::Regex;
use std::cmp;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let file = File::open("input").expect("file not found");
    let mut star_map = StarMap::new();

    let re = Regex::new(r".*=<([-\s]*\d+),([-\s]*\d+)>.*=<([-\s]*\d+),([-\s]*\d+)>").unwrap();
    for line in BufReader::new(file).lines().filter_map(|l| l.ok()) {
        let cap = re.captures(&line).unwrap();
        let x_pos = cap.get(1).unwrap().as_str().trim().parse::<i32>().unwrap();
        let y_pos = cap.get(2).unwrap().as_str().trim().parse::<i32>().unwrap();
        let x_vel = cap.get(3).unwrap().as_str().trim().parse::<i8>().unwrap();
        let y_vel = cap.get(4).unwrap().as_str().trim().parse::<i8>().unwrap();

        star_map.push(Star {
            pos: Point { x: x_pos, y: y_pos },
            vel: Velocity { x: x_vel, y: y_vel },
        })
    }

    println!("part one: {}", part_one(star_map.clone()));
}

fn part_one(mut star_map: StarMap) -> String {
    loop {
        star_map.update_all();
        if star_map.star_y_range() < 20 {
            println!("{}", star_map.print_stars());
        }
    }

    "check the terminal".to_string()
}


#[derive(Clone)]
struct StarMap {
    gen: u32,
    stars: Vec<Star>,
}

impl StarMap {
    fn new() -> StarMap {
        StarMap {
            gen: 0,
            stars: Vec::new(),
        }
    }

    fn new_from_vec(stars: Vec<Star>) -> StarMap {
        StarMap { gen: 0, stars }
    }

    fn push(&mut self, s: Star) {
        self.stars.push(s);
    }

    fn update_all(&mut self) {
        self.gen += 1;
        for s in &mut self.stars {
            s.traverse();
        }
    }

    fn star_x_range(&self) -> usize {
        let max = self.stars.iter().map(|star| star.pos.x).max().unwrap();
        let min = self.stars.iter().map(|star| star.pos.x).min().unwrap();

        (max - min) as usize
    }

    fn star_x_min(&self) -> usize {
        self.stars.iter().map(|star| star.pos.x).min().unwrap() as usize
    }

    fn star_y_range(&self) -> usize {
        let max = self.stars.iter().map(|star| star.pos.y).max().unwrap();
        let min = self.stars.iter().map(|star| star.pos.y).min().unwrap();

        (max - min) as usize
    }

    fn star_y_min(&self) -> usize {
        self.stars.iter().map(|star| star.pos.y).min().unwrap() as usize
    }

    fn step_forwards(&mut self, steps: usize) {
        for _ in 0..steps {
            self.update_all();
        }
    }

    fn print_stars(&self) -> String {
        let y_range = self.star_y_range();
        let x_range = self.star_x_range();

        let x_min = self.star_x_min();
        let y_min = self.star_y_min();
        // not coalesced enough
        if y_range > 200 {
            return "".to_string();
        }

        let mut map = vec![vec!['.'; x_range + 1]; y_range + 1];

        for star in &self.stars {
            let x = star.pos.x as usize;
            let y = star.pos.y as usize;

            map[y - y_min][x - x_min] = '#';
        }

        let mut output: String = format!("gen: {}\n", self.gen).to_owned();
        for row in map {
            let row_str: String = row.iter().collect();
            output.push_str(&row_str);
            output.push_str("\n");
        }

        output
    }
}

#[derive(Clone)]
struct Star {
    pos: Point,
    vel: Velocity,
}

impl Star {
    fn new(start: Point, vel: Velocity) -> Star {
        Star { pos: start, vel }
    }

    fn traverse(&mut self) -> &mut Self {
        self.pos.x += self.vel.x as i32;
        self.pos.y += self.vel.y as i32;
        self
    }
}

#[derive(Clone)]
pub struct Point {
    x: i32,
    y: i32,
}

#[derive(Clone)]
pub struct Velocity {
    x: i8,
    y: i8,
}
