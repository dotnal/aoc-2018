#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

lazy_static! {
    static ref RE: Regex = Regex::new(
        r"(?x)
#
(?P<id>\d{1,4})  #id
.{3}
(?P<xloc>\d{1,3}) #xloc
,
(?P<yloc>\d{1,3}) #yloc
.{2}
(?P<xdim>\d{1,3}) #xsize
x
(?P<ydim>\d{1,3}) #ysize
",
    )
    .unwrap();
}

fn main() {
    let f = File::open("input").expect("error opening file");
    let reader = BufReader::new(f);

    let mut claim_list: Vec<Claim> = Vec::new();
    let mut fabric: Fabric = Fabric::new();

    for line in reader.lines().filter_map(|i| i.ok()) {
        claim_list.push(Claim::new(&line));
    }

    for claim in &claim_list {
        fabric.add_new_claim(&claim)
    }

    println!("part one: {}", part_one(&fabric));
    println!("part two: {}", part_two(&claim_list, &fabric).unwrap());
}

fn part_one(fabric: &Fabric) -> i32 {
    fabric.count_overlap()
}

fn part_two(claim_list: &Vec<Claim>, fabric: &Fabric) -> Result<i32, String> {
    if let Some(result) = claim_list
        .iter()
        .find(|claim| !fabric.is_overlapping_claim(claim))
    {
        Ok(result.id)
    } else {
        Err("no non-overlapping claims found".to_string())
    }
}

#[derive(Debug)]
struct Claim {
    id: i32,
    l: Location,
    d: Dimensions,
}

impl Claim {
    fn new(input: &str) -> Claim {
        let caps = RE.captures(input).unwrap();
        return Claim {
            id: caps["id"].parse::<i32>().unwrap(),
            l: Location::new(
                caps["xloc"].parse::<i32>().unwrap(),
                caps["yloc"].parse::<i32>().unwrap(),
            ),
            d: Dimensions::new(
                caps["xdim"].parse::<i32>().unwrap(),
                caps["ydim"].parse::<i32>().unwrap(),
            ),
        };
    }
}

#[derive(Debug)]
struct Dimensions {
    x: i32,
    y: i32,
}
impl Dimensions {
    fn new(x: i32, y: i32) -> Dimensions {
        Dimensions { x, y }
    }
}

#[derive(Debug)]
struct Location {
    x: i32,
    y: i32,
}

impl Location {
    fn new(x: i32, y: i32) -> Location {
        Location { x, y }
    }
}

struct Fabric {
    cloth: Vec<Vec<i32>>,
}

impl Fabric {
    fn new() -> Fabric {
        Fabric {
            cloth: vec![vec![0; 1000]; 1000],
        }
    }

    fn add_new_claim(&mut self, c: &Claim) {
        let x_loc = c.l.x;
        let y_loc = c.l.y;
        let x_dim = c.d.x;
        let y_dim = c.d.y;

        for x in x_loc..x_loc + x_dim {
            for y in y_loc..y_loc + y_dim {
                self.cloth[x as usize][y as usize] += 1;
            }
        }
    }

    fn count_overlap(&self) -> i32 {
        self.cloth
            .iter()
            .flat_map(|row| row.iter().filter(|cell| **cell > 1))
            .sum()
    }

    fn is_overlapping_claim(&self, c: &Claim) -> bool {
        let x_loc = c.l.x as usize;
        let y_loc = c.l.y as usize;
        let x_dim = c.d.x as usize;
        let y_dim = c.d.y as usize;

        self.cloth[x_loc..x_loc + x_dim]
            .iter()
            .flat_map(|row| &row[y_loc..y_loc + y_dim])
            .any(|cell| *cell > 1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_overlapping_claim() {
        let mut fabric = Fabric::new();
        let claim_one = Claim {
            id: 1,
            l: Location::new(1, 1),
            d: Dimensions::new(1, 1),
        };

        fabric.add_new_claim(&claim_one);
        assert_eq!(fabric.is_overlapping_claim(&claim_one), false);

        fabric.add_new_claim(&claim_one);
        assert_eq!(fabric.is_overlapping_claim(&claim_one), true);
    }

    #[test]
    fn test_regex_captures() {
        let test_str_a: String = "#1367 @ 488,699: 27x18".to_string();

        let caps = RE.captures(&test_str_a).unwrap();
        assert_eq!(&caps["id"], "1367");
        assert_eq!(&caps["xloc"], "488");
        assert_eq!(&caps["yloc"], "699");
        assert_eq!(&caps["xdim"], "27");
        assert_eq!(&caps["ydim"], "18");
    }

    #[test]
    fn test_cloth_overlap() {
        let mut fabric = Fabric::new();
        fabric.cloth[2 as usize][3 as usize] = 4;
        assert_eq!(fabric.count_overlap(), 1);

        fabric.cloth[6 as usize][7 as usize] = 6;
        fabric.cloth[9 as usize][14 as usize] = 4;
        assert_eq!(fabric.count_overlap(), 3);
    }
}
