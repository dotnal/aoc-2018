use std::collections::HashMap;
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let f = File::open("input").expect("error opening file");
    let line = BufReader::new(f)
        .lines()
        .filter_map(|i| i.ok())
        .next()
        .unwrap();

    let words = line.split_whitespace().collect::<Vec<_>>();

    let players = words[0].parse::<usize>().unwrap();
    let point_value = words[6].parse::<usize>().unwrap();

    println!("part one: {}", part_one(players, point_value));
    println!("part two: {}", part_one(players, point_value*100));
}

fn part_one(player_count: usize, point_value: usize) -> usize {
    // why the hell didn't this work
    // let mut players: HashMap<usize, usize> = HashMap::new();
    let mut players = vec![0usize; player_count];
    let mut marble_circle: MarbleCircle<usize> = MarbleCircle::new(point_value);

    marble_circle.push_head(0);
    for current_marble in 1..=point_value {
        if (current_marble % 23) == 0 {
            let rotated_marb = marble_circle
                .rotate_counter_clockwise(7)
                .pop_head()
                .unwrap();
            let points = rotated_marb + current_marble;
            players[current_marble % player_count] += points;
        } else {
            marble_circle.rotate_clockwise(2).push_head(current_marble);
        }
    }

    *players.iter().max().unwrap()
}

struct MarbleCircle<T> {
    marbles: VecDeque<T>,
}

// HEAD is the current element
impl<T> MarbleCircle<T> {
    fn new(cap: usize) -> MarbleCircle<T> {
        MarbleCircle {
            marbles: VecDeque::with_capacity(cap + 1),
        }
    }

    // fn from_iter(iter: Iterator<Item=T>) -> MarbleCircle<T>
    // where
    //     T: IntoIterator,
    // {
    //     MarbleCircle {
    //         marbles: VecDeque::from_iter(iter),
    //     }
    // }

    fn rotate_clockwise(&mut self, steps: usize) -> &mut Self {
        for _ in 0..steps {
            let front = self.marbles.pop_front().unwrap();
            self.marbles.push_back(front);
        }
        self
    }

    fn rotate_counter_clockwise(&mut self, steps: usize) -> &mut Self {
        for _ in 0..steps {
            let back = self.marbles.pop_back().unwrap();
            self.marbles.push_front(back);
        }
        self
    }

    fn push_head(&mut self, val: T) -> &mut Self {
        self.marbles.push_front(val);
        self
    }

    fn pop_head(&mut self) -> Option<T> {
        self.marbles.pop_front()
    }

    fn peek_head(&mut self) -> Option<&T> {
        self.marbles.front()
    }

    fn get_marbles(&mut self) -> Vec<&T> {
        self.marbles.iter().collect()
    }
}

#[test]
fn do_first_steps() {
    let mut marble_circle: MarbleCircle<i32> = MarbleCircle::new(50);
    marble_circle.push_head(0);
    assert_eq!(marble_circle.get_marbles(), vec![&0]);

    marble_circle.rotate_clockwise(2).push_head(1);
    assert_eq!(marble_circle.get_marbles(), vec![&1, &0]);

    marble_circle.rotate_clockwise(2).push_head(2);
    assert_eq!(marble_circle.get_marbles(), vec![&2, &1, &0]);

    marble_circle.rotate_clockwise(2).push_head(3);
    assert_eq!(marble_circle.get_marbles(), vec![&3, &0, &2, &1]);

    marble_circle.rotate_clockwise(2).push_head(4);
    assert_eq!(marble_circle.get_marbles(), vec![&4, &2, &1, &3, &0]);

    marble_circle.rotate_clockwise(2).push_head(5);
    marble_circle.rotate_clockwise(2).push_head(6);
    marble_circle.rotate_clockwise(2).push_head(7);
    marble_circle.rotate_clockwise(2).push_head(8);
    marble_circle.rotate_clockwise(2).push_head(9);
    marble_circle.rotate_clockwise(2).push_head(10);
    marble_circle.rotate_clockwise(2).push_head(11);
    marble_circle.rotate_clockwise(2).push_head(12);
    marble_circle.rotate_clockwise(2).push_head(13);
    marble_circle.rotate_clockwise(2).push_head(14);
    marble_circle.rotate_clockwise(2).push_head(15);
    marble_circle.rotate_clockwise(2).push_head(16);
    marble_circle.rotate_clockwise(2).push_head(17);
    marble_circle.rotate_clockwise(2).push_head(18);
    marble_circle.rotate_clockwise(2).push_head(19);
    marble_circle.rotate_clockwise(2).push_head(20);
    marble_circle.rotate_clockwise(2).push_head(21);
    marble_circle.rotate_clockwise(2).push_head(22);
    assert_eq!(
        marble_circle.get_marbles(),
        vec![
            &22, &11, &1, &12, &6, &13, &3, &14, &7, &15, &0, &16, &8, &17, &4, &18, &9, &19, &2,
            &20, &10, &21, &5
        ]
    );
    let rotated_marb = marble_circle
        .rotate_counter_clockwise(7)
        .pop_head()
        .unwrap();

    assert_eq!(rotated_marb, 9);
    assert_eq!(
        marble_circle.get_marbles(),
        vec![
            &19, &2, &20, &10, &21, &5, &22, &11, &1, &12, &6, &13, &3, &14, &7, &15, &0, &16, &8,
            &17, &4, &18,
        ]
    );
    marble_circle.rotate_clockwise(2).push_head(24);
    assert_eq!(
        marble_circle.get_marbles(),
        vec![
            &24, &20, &10, &21, &5, &22, &11, &1, &12, &6, &13, &3, &14, &7, &15, &0, &16, &8, &17,
            &4, &18, &19, &2,
        ]
    );
}

#[test]
fn test_marble_rotate_clockwise() {
    let mut marble_circle: MarbleCircle<i32> = MarbleCircle::new(50);

    marble_circle.push_head(0);
    marble_circle.push_head(1);
    marble_circle.push_head(2);
    marble_circle.push_head(3);
    assert_eq!(marble_circle.peek_head(), Some(&3));

    marble_circle.rotate_clockwise(1);
    assert_eq!(marble_circle.peek_head(), Some(&2));

    marble_circle.push_head(5);
    assert_eq!(marble_circle.peek_head(), Some(&5));

    marble_circle.rotate_clockwise(2);
    assert_eq!(marble_circle.peek_head(), Some(&1));
}

#[test]
fn test_marble_rotate_counter_clockwise() {
    let mut marble_circle: MarbleCircle<i32> = MarbleCircle::new(50);

    marble_circle.push_head(0);
    marble_circle.push_head(1);
    marble_circle.push_head(2);
    marble_circle.push_head(3);
    assert_eq!(marble_circle.peek_head(), Some(&3));

    marble_circle.rotate_counter_clockwise(1);
    assert_eq!(marble_circle.peek_head(), Some(&0));

    marble_circle.push_head(5);
    assert_eq!(marble_circle.peek_head(), Some(&5));

    marble_circle.rotate_counter_clockwise(2);
    assert_eq!(marble_circle.peek_head(), Some(&2));
}

#[test]
fn part_one_test_cases() {
    assert_eq!(part_one(10, 1618), 8317);
    assert_eq!(part_one(13, 7999), 146373);
    assert_eq!(part_one(17, 1104), 2764);
    assert_eq!(part_one(21, 6111), 54718);
    assert_eq!(part_one(30, 5807), 37305);
}
