use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> Result<(), String> {
    println!("part one: {}", part_one()?);
    println!("part two: {}", part_two()?);
    Ok(())
}

fn part_one() -> Result<i32, String> {
    let f = File::open("input").expect("error opening file");
    let reader = BufReader::new(f);

    let mut two_count: i32 = 0;
    let mut three_count: i32 = 0;

    for line in reader.lines().filter_map(|i| i.ok()) {
        let mut letter_count: HashMap<char, i32> = HashMap::new();

        for c in line.chars() {
            letter_count.entry(c).and_modify(|v| *v += 1).or_insert(1);
        }

        if letter_count.iter().filter(|(_, v)| **v == 2).count() > 0 {
            two_count += 1;
        }

        if letter_count.iter().filter(|(_, v)| **v == 3).count() > 0 {
            three_count += 1;
        }
    }

    Ok(two_count * three_count)
}

fn part_two() -> Result<String, String> {
    let f = File::open("input").expect("error opening file");
    let reader = BufReader::new(f);
    let mut id_list: Vec<_> = reader.lines().filter_map(|i| i.ok()).collect();

    loop {
        if id_list.is_empty() {
            return Err("exhausted all ids".to_string());
        }

        let id = id_list.pop().unwrap();

        for id_two in &id_list {
            if soft_match(&id, id_two)? {
                return Ok(dediff(&id, id_two)?);
            }
        }
    }
}

fn soft_match(id_one: &String, id_two: &String) -> Result<bool, String> {
    if id_one.len() != id_two.len() {
        return Err("ids are different length".to_string());
    }

    Ok(1 >= id_one
        .chars()
        .zip(id_two.chars())
        .filter(|(c, c2)| c != c2)
        .count())
}

fn dediff(id_one: &String, id_two: &String) -> Result<String, String> {
    if id_one.len() != id_two.len() {
        return Err("ids are different length".to_string());
    }

    let (result, _): (String, String) = id_one
        .chars()
        .zip(id_two.chars())
        .filter(|(c, c2)| c == c2)
        .unzip();

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_soft_match() {
        let string_a = "asdfghjkl".to_string();
        let string_b = "asdfgujkl".to_string();

        assert!(soft_match(&string_a, &string_b).unwrap())
    }

    #[test]
    fn test_non_soft_match() {
        let string_a = "asdfghjkl".to_string();
        let string_b = "asefgujkl".to_string();

        assert!(!soft_match(&string_a, &string_b).unwrap())
    }

    #[test]
    fn test_dediff() {
        let string_a = "asdfghjkl".to_string();
        let string_b = "asdfgujkl".to_string();

        assert_eq!(dediff(&string_a, &string_b).unwrap(), "asdfgjkl".to_string())
    }
}
