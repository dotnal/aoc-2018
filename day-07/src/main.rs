use std::cmp;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::collections::{HashMap, HashSet};
fn main() {
    let file = File::open("input").expect("file not found");
    let mut step_map = StepMap::new();

    for line in BufReader::new(file).lines().filter_map(|l| l.ok()) {
        let chars = line.split_whitespace().collect::<Vec<_>>();
        let step = chars[7].chars().next().unwrap() as char;
        let depends_on: char = chars[1].chars().next().unwrap() as char;
        step_map.push(step, depends_on);
    }

    println!("part one: {}", part_one(step_map.clone()));
    println!("part two: {}", part_two(step_map.clone()));
}

fn part_one(mut step_map: StepMap) -> String {
    let mut steps_required = Vec::new();
    while !step_map.steps.is_empty() {
        let next_step = step_map.pop_next_unblocked().unwrap();
        steps_required.push(next_step);
    }

    steps_required.iter().map(|step| step.id).collect::<String>()
}

fn part_two(mut step_map: StepMap) -> u64 {
    let mut time_elapsed: u64 = 0;

    while !step_map.steps.is_empty() {
        let mut unblocked_steps = step_map.get_unblocked();
        let work_quant = cmp::min(5, unblocked_steps.len());

        // do work
        let mut ids_to_pop: Vec<char> = Vec::new();
        for item in unblocked_steps.iter_mut().take(work_quant) {
            if item.do_work() {
                ids_to_pop.push(item.id);
            }
        }

        for id in ids_to_pop {
            step_map.pop(id);
        }

        time_elapsed += 1;
    }

    time_elapsed
}

#[derive(Clone)]
struct StepMap {
    steps: HashMap<char, Step>,
}

impl StepMap {
    fn new() -> StepMap {
        StepMap {
            steps: HashMap::new(),
        }
    }

    fn push(&mut self, step: char, depends_on: char) {
        self.steps
            .entry(depends_on)
            .or_insert_with(|| Step::new(depends_on));
        if let Some(entry) = self.steps.get_mut(&step) {
            entry.add_dependency(depends_on);
        } else {
            let mut s = Step::new(step);
            s.add_dependency(depends_on);
            self.steps.insert(step, s);
        }
    }

    fn pop(&mut self, step: char) -> Option<Step> {
        self.steps.iter_mut().for_each(|(_, s)| s.clear_dependency(step));
        self.steps.remove(&step)
    }

    fn get_unblocked(&mut self) -> Vec<&mut Step> {
        let mut unblocked = self.steps
            .iter_mut()
            .filter(|(_, step)| step.is_unblocked())
            .map(|(_, step)| step)
            .collect::<Vec<&mut Step>>();

        unblocked.sort_unstable_by(|a, b| (a.id).cmp(&b.id));
        unblocked
    }


    fn pop_next_unblocked(&mut self) -> Option<Step> {
        let id = self
            .steps
            .iter()
            .filter(|(_, step)| step.is_unblocked())
            .map(|(c, _)| c)
            .min();

        match id {
            Some(&s) => {
                self.steps.iter_mut().for_each(|(_, step)| step.clear_dependency(s));
                self.steps.remove(&s)
            }
            None => None,
        }
    }
}

#[derive(Clone, Debug)]
struct Step {
    pub id: char,
    work_left: u8,
    dependents: HashSet<char>,
}

impl Step {
    fn new(id: char) -> Step {
        Step {
            id,
            dependents: HashSet::new(),
            work_left: 60 + (id as u8 - 64)
        }
    }

    fn add_dependency(&mut self, depends_on: char) {
        self.dependents.insert(depends_on);
    }

    fn clear_dependency(&mut self, depends_on: char) {
        self.dependents.remove(&depends_on);
    }

    fn is_unblocked(&self) -> bool {
        self.dependents.is_empty()
    }

    fn do_work(&mut self) -> bool {
        self.work_left -= 1;
        self.work_left == 0
    }
}
