use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::Error;

fn main() {
    println!("part 1: {}", part_one());
    println!("part 2: {}", part_two().unwrap());
}

fn part_one() -> i32 {
    let f = File::open("input").expect("file not found!");
    let reader = BufReader::new(f);
    reader
        .lines()
        .map(|i| i.unwrap().parse::<i32>().unwrap())
        .sum()
}

fn part_two() -> Result<i32, Error> {
    let mut f = File::open("input")?;
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    let lines = contents.lines().cycle();
    let mut counter = 0;
    let mut seen_map: HashSet<i32> = HashSet::new();

    lines
        .map(|i| i.parse::<i32>().unwrap())
        .find(|count| {
            counter += count;
            !seen_map.insert(counter)
        });

    Ok(counter)
}
